//
// const {src, dest, series, watch} = require('gulp');
// const rename = require('gulp-rename');
// const del = require('del');
// const browserSync = require('browser-sync').create();
//
// function build() {
//     return src('./page.html')
//         .pipe(rename('index.html'))
//         .pipe(dest('./built/'));
// }
//
// function cleanBuild() {
//     return del('./built');
// }
//
// function reloadOnChange() {
//     browserSync.init({
//         server: './built'
//     });
//     watch(['./page.html']).on('change',series(build, browserSync.reload));
// }
//
// exports.default = series(cleanBuild, build);
// exports.dev123 = series(build, reloadOnChange);
//

// const {src, dest} = require('gulp');
// const sass = require('gulp-sass');
// sass.compiler = require('node-sass');
//
// function copyHtml() {
//     return src('./src/index.html')
//         .pipe(dest('./dist'))
// }
// exports.html = copyHtml;
// exports.default = () => src(
//     "./src/scss/style.scss"
// ).pipe(sass()).pipe(dest("./dist/css/"));

const {src, dest, series, parallel, watch} = require('gulp'); // added watch here
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const postcss = require('gulp-postcss');
const uncss = require('postcss-uncss');
const del = require('del');
const browserSync = require('browser-sync').create();

function copyHtml() {
    return src('./src/index.html')
        .pipe(dest('./dist'))
        .pipe(browserSync.reload({ stream: true })); // added this line
}

function copyJs() {
    return src('./src/js/**.js')
        .pipe(dest('./dist/js'))
        .pipe(browserSync.reload({ stream: true })); // added this line
}

function buildScss() {
    return src('./src/scss/*.scss')
        .pipe(sass())
        .pipe(postcss([
            uncss({html: ['./dist/index.html']})]))
        .pipe(dest('./dist/css'))
        .pipe(browserSync.stream());  // added this line
}

function copyAssets() {
    return src('./src/assets/**')
        .pipe(dest('./dist/assets'))
}

function emptyDist() {
    return del('./dist/**');
}

// moved build configuration to variable
const build = series(
    emptyDist,
    parallel(
        series(copyHtml , buildScss),
        copyAssets,
        copyJs
    )
);

// added this function to start server and watch for index.html, scss, js
function serve() {
    browserSync.init({
        server: './dist'
    });

    watch(['./src/index.html'], copyHtml);
    watch(['./src/scss/*.scss'], buildScss);
    watch(['./src/js/*.js'], copyJs);
}

exports.html = copyHtml;
exports.scss = buildScss;
exports.assets = copyAssets;
exports.clear = emptyDist;
exports.js = copyJs;
exports.build = build; // changed here with variable from line 40
exports.default = series(build, serve); // added this
